import sys
import argparse
import urllib.request
import requests
import re
from bs4 import \
    BeautifulSoup
from flask import Flask, render_template, request, flash, redirect, url_for,jsonify,session,send_file,abort,make_response
import os
from flask_sqlalchemy import SQLAlchemy
import mysql.connector
from flask_marshmallow import Marshmallow
app = Flask(__name__)
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
app.config['SECRET_KEY'] = 'Covid_app!'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:root2020@localhost:3306/bail'
db = SQLAlchemy(app)
ma = Marshmallow(app)
UPLOAD_FOLDER = 'static/image_data'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
path = os.getcwd()

conn = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="root2020",
    database="bail",
    buffered=True,

)

################## All model here ###############################################
#----------------- class for detainee info ------------------------------------
class Detainee_info(db.Model):
    __tablename__ = 'detainee_info'
    print("detainee_info class call")
    detainee_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.VARCHAR(50))
    address = db.Column(db.VARCHAR(255))
    city_zip = db.Column(db.VARCHAR(255))
    detainee_code = db.Column(db.VARCHAR(45))
    age_at_booking = db.Column(db.VARCHAR(45))
    race = db.Column(db.VARCHAR(45))
    sex = db.Column(db.VARCHAR(45))
    eyes = db.Column(db.VARCHAR(45))
    hair = db.Column(db.VARCHAR(45))
    height = db.Column(db.VARCHAR(45))
    weight = db.Column(db.VARCHAR(45))
    booking_date = db.Column(db.VARCHAR(45))
    booking_time = db.Column(db.VARCHAR(45))
    arresting_agency =db.Column(db.VARCHAR(45))
    img_path = db.Column(db.VARCHAR(45))
    def __init__(self, detainee_id, name, address, city_zip, detainee_code, age_at_booking, race, sex,eyes,hair,height,weight,booking_date,booking_time,arresting_agency,img_path):
        self.detainee_id = detainee_id
        self.name = name
        self.address = address
        self.city_zip = city_zip
        self.detainee_code = detainee_code
        self.age_at_booking = age_at_booking
        self.race = race
        self.sex = sex
        self.eyes = eyes
        self.hair = hair
        self.height = height
        self.weight = weight
        self.booking_date = booking_date
        self.booking_time = booking_time
        self.arresting_agency = arresting_agency
        self.img_path = img_path


    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return (self.detainee_id)


#----------------- class for Detainee_charges management ------------------------------------
class Detainee_charges(db.Model):
    __tablename__ = 'detainee_charges'
    print("detainee_charges class call")
    detainee_id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.VARCHAR(100))
    charge_start = db.Column(db.VARCHAR(45))
    charge_end = db.Column(db.VARCHAR(45))
    department = db.Column(db.VARCHAR(100))
    court = db.Column(db.VARCHAR(100))
    date = db.Column(db.VARCHAR(45))
    time = db.Column(db.VARCHAR(45))
    bond = db.Column(db.VARCHAR(45))


    def __init__(self, detainee_id, description, charge_start, charge_end, department,court,date,time,bond):
        self.detainee_id = detainee_id
        self.description = description
        self.charge_start = charge_start
        self.charge_end = charge_end
        self.department = department
        self.court = court
        self.date = date
        self.time = time
        self.bond = bond

################################ALL Schema #########################################
#---------------- detainee info schema class --------------------------------------------
class Detainee_infoSchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ('detainee_id', 'name', 'address', 'city_zip', 'detainee_code', 'age_at_booking', 'race', 'sex', 'eyes', 'hair', 'height', 'weight' ,'booking_date', 'booking_time', 'arresting_agency','img_path')
detainee_info_schema = Detainee_infoSchema()
detainees_info_schema = Detainee_infoSchema(many=True)

#---------------- detainee charges schema class --------------------------------------------
class Detainee_chargesSchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ('detainee_id', 'description', 'charge_start', 'charge_end', 'department','court','date','time','bond')
detainee_charges_schema = Detainee_chargesSchema()
detainees_charges_schema = Detainee_chargesSchema(many=True)

# ------------------api for extraction -----------------------------------------------------------------------------
@app.route('/', methods=['GET', 'POST'])
def index():
    print("index api")
    return redirect(url_for('extraction'))
# url table = 'https://www.so.washington.ar.us/res/DAlphaRoster.aspx'
@app.route('/extraction',methods=['GET', 'POST'])
def extraction():
    conn.reconnect()
    print("extraction api")
    if request.method == 'POST':
        print("post request")
        flash('URL Already Extracted')
    #     #------------------------------------main frame url ------------------------------------------------------------------
    #     table_url = request.form['table_url']
    #     r = urllib.request.urlopen(table_url)
    #     soup = BeautifulSoup(r, "html.parser")
    #     # print(soup)
    #     tabl = soup.find_all(id='Results')
    #     # print(tabl)
    #     # -----------------------find all anchor tag ---------------------------------------------------
    #     anchors = soup.find_all('a')
    #     all_link =set()
    #     for link in anchors:
    #         if link.get('href') != '#':
    #             link_text = "https://www.so.washington.ar.us/res/" + link.get('href')
    #             all_link.add(link_text)
    #     print(all_link)
    #     print(len(all_link))
    #     for url_link in all_link:
    #         # url = 'https://www.so.washington.ar.us/res/Detainee.aspx?bn=4218781'
    #         count2 = 0
    #
    #         r1 = urllib.request.urlopen(url_link)
    #         soup1 = BeautifulSoup(r1, "html.parser")
    #         # print(soup1)
    #         name = soup1.find(id='ContentPlaceHolder1_lblName').get_text()
    #         print("name",name)
    #         print("type name",type(name))
    #         address = soup1.find(id='ContentPlaceHolder1_lbladdress').get_text()
    #         print("address",address)
    #         city_zip = soup1.find(id='ContentPlaceHolder1_lblcityzip').get_text()
    #         print("city_zip",city_zip)
    #         # ---------------------find detain ID - ---------------------------------------
    #         detained_id = ""
    #         table_td_detain = soup1.find_all('td')
    #         for t in table_td_detain:
    #             # print("t",t.contents)
    #             for k in t.contents:
    #                 # print("k")
    #                 if 'Detainee' in k:
    #                     detain =str(k).replace(" ","")
    #                     # print("detain",detain)
    #                     s = detain.split(":")
    #                     print("s[1]",s[1])
    #                     detained_id=s[1]
    #         print("detaine ID",detained_id)
    #         print("type detainee id",type(detained_id))
    #         # -------------------------------------------------------------------------
    #         age_at_booking = soup1.find(id='ContentPlaceHolder1_lblage').get_text()
    #         print("age_at_booking",age_at_booking)
    #         race = soup1.find(id='ContentPlaceHolder1_lblRace').get_text()
    #         print("race",race)
    #         sex = soup1.find(id='ContentPlaceHolder1_lblSex').get_text()
    #         print("sex",sex)
    #         eyes = soup1.find(id='ContentPlaceHolder1_lbleyes').get_text()
    #         print("eyes",eyes)
    #         hair = soup1.find(id='ContentPlaceHolder1_lblhair').get_text()
    #         print("hair",hair)
    #         height = soup1.find(id='ContentPlaceHolder1_lblheight').get_text()
    #         print("height",height)
    #         weight= soup1.find(id='ContentPlaceHolder1_lblweight').get_text()
    #         print("weight",weight)
    #         booking_date = soup1.find(id='ContentPlaceHolder1_lblidate').get_text()
    #         print("booking_date",booking_date)
    #         booking_time = soup1.find(id='ContentPlaceHolder1_lblitime').get_text()
    #         print("booking_time",booking_time)
    #         arresting_agency = soup1.find(id='ContentPlaceHolder1_lblArrestAgency').get_text()
    #         print("arresting_agency",arresting_agency)
    #
    #         # ---------------------------------find table data after 24 --------------------------------------------------------------
    #         table_td= soup1.find_all('td')
    #         td_data =[]
    #         counter=0
    #         for f in table_td:
    #             # print("f",f.contents)
    #             if counter>=36:
    #                 ls=(f.get_text())
    #                 td_data.append(ls)
    #             counter = counter+1
    #         # print("td_data",td_data)
    #         # print(len(td_data))
    #         lista = [el.replace('\xa0','') for el in td_data]
    #         print("lista",lista)
    #         print("len(lista)",len(lista))
    #
    #         # --------------------------------find image ------------------------------------------------------------
    #         img = soup1.find_all('img')
    #         # print(img)
    #         img_url = ""
    #         file_name = ""
    #         for k in img:
    #             if 'getimage' in k['src']:
    #                 img_url='https://www.so.washington.ar.us/res/'+k['src']
    #                 file = img_url.split("=")
    #                 file_name=file[1]
    #                 print("filename",file_name)
    #         print("img_url",img_url)
    #
    #         save_file= urllib.request.urlretrieve(img_url,"static/image_data/"+file_name+".jpg")
    #         print("save_file",save_file)
    #         path = 'static/image_data/'+file_name+'.jpg'
    #         print("path type",type(path))
    #
    # #         -----------------------------------------insert into database =---------------------------------------------
    #         mycursor = conn.cursor()
    #         sql = "INSERT INTO detainee_info(name, address, city_zip, detainee_code, age_at_booking, race, sex,eyes,hair,height,weight,booking_date,booking_time,arresting_agency,img_path) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
    #         val = (name,address,city_zip,str(detained_id),age_at_booking,race,sex,eyes,hair,height,weight,booking_date,booking_time,arresting_agency,path)
    #         mycursor.execute(sql, val)
    #         conn.commit()
    #         sql_query = "select * from detainee_info where detainee_code='" + detained_id + "'"
    #         cur2 = conn.cursor()
    #         result = cur2.execute(sql_query)
    #         result = cur2.fetchall()
    #         conn.commit()
    #         print("result get ",result)
    #         detainee_ext_id = ""
    #         if len(result) >= 1:
    #             print("result for loop")
    #             r = result[0]
    #             detainee_ext_id = r[0]
    #             print("result user id =", detainee_ext_id)
    #         print("detain_id outside loop",detainee_ext_id)
    #         # -----------------------------------insert into detainee charges---------------------------------------
    #         def divide_chunks(l, n):
    #             # looping till length l
    #             for i in range(0, len(l), n):
    #                 yield l[i:i + n]
    #                 # How many elements each
    #         # list should have
    #         n = 8
    #         x = list(divide_chunks(lista, n))
    #         print("loop of 8 data",x)
    #         for j in x:
    #             sql_query = "insert into detainee_charges(detainee_id, description, charge_start, charge_end, department,court,date,time,bond)values(%s,%s,%s,%s,%s,%s,%s,%s,%s)"
    #             cur = conn.cursor()
    #             val = (detainee_ext_id,j[0],j[1],j[2],j[3],j[4],j[5],j[6],j[7])
    #             cur.execute(sql_query, val)
    #             conn.commit()
    #         count2 = count2 + 1
    #         print("count",count2)
        return redirect(url_for('extraction'))
    else:
        extract_url = 'https://www.so.washington.ar.us/res/DAlphaRoster.aspx'
        return render_template('index.html', extract_url=extract_url)

# ------------------api for show result -----------------------------------------------------------------------------
@app.route('/result', methods=['GET', 'POST'])
def result():
    conn.reconnect()
    print("index api")
    if request.method == 'POST':
        print("post result")
        d_id = request.form['state']
        return redirect(url_for('.result', d_id=d_id))
    else:
        rs = Detainee_info.query.all()
        d1_id = request.args.get('d_id')
        print("d1_id",d1_id)
        info = detainees_info_schema.dump(rs)
        print("info",info)
        ps = Detainee_charges.query.all()
        charges = detainees_charges_schema.dump(ps)
        print("charges",charges)
        all_data = zip(info,charges)
        d_in = Detainee_info.query.filter_by(detainee_id=d1_id).all()
        d_info = detainees_info_schema.dump(d_in)
        print("d_info",d_info)
        n_charges =[]
        # for j in Detainee_charges.query.filter_by(detainee_id=d1_id).with_entities(Detainee_charges.detainee_id).all():
        #     d_ch = Detainee_charges.query.filter_by(detainee_id=d1_id).with_entities(Detainee_charges.detainee_id).all()
        #     d_id = detainees_charges_schema.dump(d_ch)
        #     print("d_id",d_id)
        #     n_charges.append(d_id)
        # #     new data------------------------------------------
        #     d_ch1 = Detainee_charges.query.filter_by(detainee_id=d1_id).with_entities(Detainee_charges.description).all()
        #     description = detainees_charges_schema.dump(d_ch1)
        #     print("description", description)
        #     n_charges.append(description)
        # #     new data------------------------------------------
        #     d_ch2 = Detainee_charges.query.filter_by(detainee_id=d1_id).with_entities(Detainee_charges.charge_start).all()
        #     charge_start = detainees_charges_schema.dump(d_ch2)
        #     print("charge_start", charge_start)
        #     n_charges.append(charge_start)
        # #     new data------------------------------------------
        #     d_ch3 = Detainee_charges.query.filter_by(detainee_id=d1_id).with_entities(Detainee_charges.charge_end).all()
        #     charge_end = detainees_charges_schema.dump(d_ch3)
        #     print("charge_end", charge_end)
        #     n_charges.append(charge_end)
        # #     new data------------------------------------------
        #     d_ch4 = Detainee_charges.query.filter_by(detainee_id=d1_id).with_entities(Detainee_charges.department).all()
        #     department = detainees_charges_schema.dump(d_ch4)
        #     print("department", department)
        #     n_charges.append(department)
        # #     new data------------------------------------------
        #     d_ch5 = Detainee_charges.query.filter_by(detainee_id=d1_id).with_entities(Detainee_charges.court).all()
        #     court = detainees_charges_schema.dump(d_ch5)
        #     print("court", court)
        #     n_charges.append(court)
        # #     new data------------------------------------------
        #     d_ch6 = Detainee_charges.query.filter_by(detainee_id=d1_id).with_entities(Detainee_charges.date).all()
        #     date = detainees_charges_schema.dump(d_ch6)
        #     print("date", date)
        #     n_charges.append(date)
        # #     new data------------------------------------------
        #     d_ch7 = Detainee_charges.query.filter_by(detainee_id=d1_id).with_entities(Detainee_charges.time).all()
        #     time = detainees_charges_schema.dump(d_ch7)
        #     print("time", time)
        #     n_charges.append(time)
        # #     new data------------------------------------------
        #     d_ch8 = Detainee_charges.query.filter_by(detainee_id=d1_id).with_entities(Detainee_charges.bond).all()
        #     bond = detainees_charges_schema.dump(d_ch8)
        #     print("bond", bond)
        #     n_charges.append(bond)
        # print("n_charges",n_charges)

        if d1_id != None:
            sql_query = "select detainee_charges.description,detainee_charges.charge_start,detainee_charges.charge_end,detainee_charges.department,detainee_charges.court,detainee_charges.date,detainee_charges.time,detainee_charges.bond from detainee_charges join detainee_info on detainee_charges.detainee_id = detainee_info.detainee_id where detainee_charges.detainee_id='"+d1_id+"'"
            cur2 = conn.cursor()
            result = cur2.execute(sql_query)
            result = cur2.fetchall()
            conn.commit()
            print("result",result)
            return render_template('result.html', info=info, d_info=d_info, result=result)
        return render_template('result.html', info=info, d_info=d_info)


if __name__ == "__main__":
    app.run(host='0.0.0.0',port=5000)